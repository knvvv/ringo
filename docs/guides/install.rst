.. _installation-label:

Installation
============

Make sure that you are on Linux (x86\_64) and your Python version is \>= 3.8

.. code-block:: bash

    $ python --version
    Python 3.12.2
    # In this example, 3.12 is newer than 3.8

Ringo can be installed using this command:

.. code-block:: bash

    pip install --upgrade ringo-ik

Test your installation:

.. code-block:: python

    >>> import ringo.test
    >>> ringo.test.run_tests()
    Running tests...
    All tests completed successfully:
    ✔ 1) manual_dofs (OK)
    ✔ 2) mol_stats (OK)
    ✔ 3) all_solutions (OK)
    ✖ 4) sampling_singlethread (Skipped)
    ✖ 5) sampling_parallel (Skipped)
    Running tests...
    All tests completed successfully:
    ✔ 1) load_hydrogen
    ✔ 2) empty_filter
    ✔ 3) description_edit
    ✔ 4) filtering
    ✔ 5) sorting
    ✔ 6) attrs
    ✔ 7) rmsd
    ✔ 8) rmsd_pair_iter
    ✔ 9) molgraph
    ✔ 10) geom_analysis
    >>> 

Note that the tests ``sampling_singlethread`` and ``sampling_parallel`` are skipped. This is because the built-in conformational sampling module (MCR, Monte-Carlo with Refinement) is propriatory and currently it's not included in PyPi Ringo release.