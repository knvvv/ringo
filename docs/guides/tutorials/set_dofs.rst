Generate conformation for given dihedrals
=========================================

The ``Molecule`` class is at the heart of ``Ringo``'s functionality, providing a simple interface for manipulating a molecule's degrees of freedom. The ``Molecule`` object can be initialized with either an SDF file (starting conformer is required) or NetworkX graph (starting conformer is not required) which both provide all necessary structural and bonding information of the molecule. Calling the ``get_ps`` method returns a list of degrees of freedom tuples and their corresponding values as a numpy array (``get_discrete_ps`` returns a similar set of discrete DOFs, i.e. IK solution indices). New values of these degrees of freedom can be assigned to the numpy array and set via ``apply_ps`` method to generate new conformations. Here, we'll use initialization from SDF.

One random conformation
-----------------------

.. code-block:: python

    import ringo
    import random
    import math
    import numpy as np

    NUM_TRIES = 10000

    # Load and analyze molecular topology
    mol = ringo.Molecule(sdf='test_sets/macromodel/start_structures/pdb_1NWX.sdf')
    print(
        f"Basic info about the molecule: {repr(ringo.get_molecule_statistics(mol))}"
    )

    # Read PyXYZ manual if you don't know what Confpool is
    # https://knvvv.gitlab.io/pyxyz
    p = ringo.Confpool()

    dofs_list, dofs_values = mol.get_ps()
    # * dofs_list is a list of Tuple[int, int, int, int] of atom indices
    # * dofs_values is a numpy array whose buffer will
    #   be read when mol.apply_ps() is called
    ddofs_list, ddofs_values = mol.get_discrete_ps()
    # * ddofs_list is a list of Set[int] of ring atom indices
    # * ddofs_values is a numpy array whose buffer will
    #   be read when mol.apply_ps() is called
    for i in range(NUM_TRIES):
        for i, cur_param in enumerate(dofs_list):
            newvalue = random.uniform(-math.pi, math.pi)
            dofs_values[i] = newvalue

        for i, cur_param in enumerate(ddofs_list):
            ddofs_values[i] = -1  # -1 requests for random solution of IK

        # Attempt to generate a conformer from the values of
        # dihedrals and solution indices
        result: int = mol.apply_ps()
        # 0 - success
        # 1 - zero (Not inverse kinematics solutions for given dihedrals)
        # 2 - tlcfail (Numerical error in inverse kinematics solver)
        # 3 - overlap (Didn't pass overlap checks)
        # 4 - validationfail (Didn't pass bond length/valence angle checks)

        if result != 0:
            # If not successfull, then try another set of DOFs
            continue

        # Access geometry and store it in Confpool container
        symbols: list[str] = mol.get_symbols()
        coords: np.ndarray = mol.get_xyz()
        p.include_from_xyz(coords, f'Conformation #{len(p) + 1}')
        p.atom_symbols = symbols

    assert len(p) > 0, f'No conformers generated in {NUM_TRIES} trials'
    p.save_xyz('result.xyz')

To generate a molecule's conformation in ``Ringo``, you need to uniquely define it. This means providing the values of the torsion angles in the ``dofs_values`` array and the inverse kinematics solution index for each ring in the ``ddofs_values`` array. The torsion angles are straightforward, but the solution indices are less clear. In general, there can be anywhere from 0 to 16 solutions for each ring. For example, in a six-membered ring, 0 might correspond to the chair conformation, 1 to the boat conformation, and so on. Moreover, it's impossible to know in advance how many solutions there will be or what they represent. Therefore, if the task is to generate a random conformation, you can set the solution number to -1, which means that for each ring, a random ring conformation will be chosen from all generated solutions.

If you're wondering why we set something in ``dofs_values`` and ``ddofs_values`` arrays but don't pass them back into ``apply_ps`` - that's a good question. When ``get_ps`` and ``get_discrete_ps`` are called, the ``Molecule`` object creates an ``np.ndarray`` and retains a pointer to its buffer, so it can read the data from there later when it enters ``apply_ps``.

If you don't know what ``Confpool()`` is or how to work with it, that is explained in the `PyXYZ's manual <https://knvvv.gitlab.io/pyxyz>`_. Pyxyz is already integrated into ``Ringo`` library for convenience.

All possible conformations
--------------------------

To obtain all geometries corresponding to the specified dihedral angle values, minimal changes to the previous script are required:

.. code-block:: python

    import ringo
    import random
    import math
    import numpy as np

    NUM_TRIES = 100000

    mol = ringo.Molecule(sdf='test_sets/macromodel/start_structures/pdb_1NWX.sdf')

    p = ringo.Confpool()

    dofs_list, dofs_values = mol.get_ps()
    for i in range(NUM_TRIES):
        for i, cur_param in enumerate(dofs_list):
            newvalue = random.uniform(-math.pi, math.pi)
            dofs_values[i] = newvalue

        result = mol.prepare_solution_iterator()  # instead of .apply_ps()
        # 0 - success
        # 1 - zero (Not inverse kinematics solutions for given dihedrals)
        # 2 - tlcfail (Numerical error in inverse kinematics solver)
        # 3 - overlap (Didn't pass overlap checks)
        # 4 - validationfail (Didn't pass bond length/valence angle checks)
        # 5 - multiple_fails (All assembly sequences failed for different reasons)

        if result != 0:
            continue

        sol_list: list[np.ndarray] = mol.get_solutions_list()
        for i, matr in enumerate(sol_list):
            p.include_from_xyz(matr, f"Solution #{i}")
        p.atom_symbols = mol.get_symbols()
        p.save_xyz('single_iteration.xyz')
        break

    assert len(p) > 0, f'No conformers generated in {NUM_TRIES} trials'

There are a few differences from generating a random conformer. First, you don't need to call ``mol.get_discrete_ps()`` and set the values to -1. Second, instead of ``mol.apply_ps()``, you need to call ``mol.prepare_solution_iterator()``. Third, to obtain the geometries as NumPy matrices, you should use ``mol.get_solutions_list()`` instead of ``mol.get_xyz()``, and you'll get a list with all the geometries at once.
