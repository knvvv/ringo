Welcome to Ringo's documentation!
=================================

The Python Library for Kinematics-Driven Conformer Generation

Links
-----

.. list-table::
   :widths: 20 20
   :align: center

   * - .. image:: https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQc5d3d6LQOo6M0lm9qsQ-dQcVXRYtwbk2HFWe0x365rLEfUxcKqoOB7TFkVBkk2Rco1qI&usqp=CAU
         :width: 20%
     -  .. image:: https://pypi.org/static/images/logo-small.8998e9d1.svg
         :width: 20%
   * - `Ringo GitLab page <https://gitlab.com/knvvv/ringo>`_
     - `Ringo PyPi page <https://pypi.org/project/ringo-ik/>`_


References
----------

Papers on inverse kinematics conformer generation using Ringo:

#. Krivoshchapov, N. V.; Medvedev, M. G. Ring Kinematics-Informed Conformation Space Exploration. WIREs Computational Molecular Science 2024, 14 (1), e1690. https://doi.org/10.1002/wcms.1690

#. Krivoshchapov, N. V.; Medvedev, M. G. Accurate and Efficient Conformer Sampling of Cyclic Drug-Like Molecules with Inverse Kinematics. J. Chem. Inf. Model. 2024, 64 (11), 4542–4552. https://doi.org/10.1021/acs.jcim.3c02040

Ringo is based on inverse kinematics solver called TLC:

Coutsias EA, Seok C, Jacobson MP, Dill KA. A kinematic view of loop closure. J Comput Chem. 2004 Mar;25(4):510-28. doi: https://doi.org/10.1002/jcc.10416

.. toctree::
   :caption: Contents:

   guides/purpose
   guides/install
   guides/tutorials
   source/ringo

