Detailed reference
==================

Main module
-----------

.. automodule:: ringo
   :members:
   :undoc-members:
   :show-inheritance:


ringo.ringo\_base module
------------------------

These are the contents of Ringo's C++ module

.. automodule:: ringo.ringo_base
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   ringo.pyutils
   ringo.pyxyz
   ringo.test